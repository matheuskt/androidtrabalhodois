package br.com.matheus.trabalhodois.entity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import br.com.matheus.trabalhodois.R;

public class MutantCell extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> mutants;
    private final ArrayList<String> images;

    public MutantCell(Activity context, ArrayList<String> mutants,ArrayList<String> imageStrings) {
        super(context, R.layout.mutant_cell, mutants);
        this.context = context;
        this.mutants = mutants;
        this.images = imageStrings;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.mutant_cell, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imgView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(mutants.get(position));
        new DownloadImageTask(imgView).execute(images.get(position));
        return rowView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
