package br.com.matheus.trabalhodois.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import br.com.matheus.trabalhodois.R;
import br.com.matheus.trabalhodois.entity.Ability;
import br.com.matheus.trabalhodois.entity.Mutant;
import br.com.matheus.trabalhodois.services.MutantsRest;
import br.com.matheus.trabalhodois.services.VolleyCallback;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class CreateActivity extends AppCompatActivity {

    MutantsRest _mutantsRest;

    final int CODE_GALLERY_REQUEST = 999;

    @BindView(R.id.btnSave) Button _btnSave;
    @BindView(R.id.inputName) EditText _inputName;
    @BindView(R.id.habilidade1) EditText _ability1;
    @BindView(R.id.habilidade2) EditText _ability2;
    @BindView(R.id.habilidade3) EditText _ability3;
    @BindView(R.id.btnAddImage) Button _btnAddImage;
    @BindView(R.id.imageUpload) ImageView _imageUpload;

    Bitmap imageBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);
        _mutantsRest = new MutantsRest();

        _btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        _btnAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(
                        CreateActivity.this,
                        new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                        CODE_GALLERY_REQUEST
                );
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void save() {

        String name = _inputName.getText().toString();
        String ab1 = _ability1.getText().toString();
        String ab2 = _ability2.getText().toString();
        String ab3 = _ability3.getText().toString();

        if (!validate()) return;

        final ProgressDialog progressDialog = new ProgressDialog(CreateActivity.this, R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Cadastrando novo mutante...");
        progressDialog.show();

        Mutant mutant = new Mutant(name);
        if (!ab1.isEmpty()) mutant.addAbility(new Ability(ab1));
        if (!ab2.isEmpty()) mutant.addAbility(new Ability(ab2));
        if (!ab3.isEmpty()) mutant.addAbility(new Ability(ab3));

        String imageString = this.imageToBase64(imageBitmap);
        mutant.setImagePath(imageString);

        _mutantsRest.store(new VolleyCallback() {
            @Override
            public void onSuccess(Integer code, String message) {
                progressDialog.dismiss();
                Log.d("Salvou mutante", "");
                onStoreSuccess();
            }

            @Override
            public void onError(int code) {
                progressDialog.dismiss();
                Log.d("Erro ao salvar", "");
                onStoreError();
            }
        }, this, mutant);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == CODE_GALLERY_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Selecionar imagem"), CODE_GALLERY_REQUEST);
            } else {
                Toast.makeText(this, "Você não tem permissão para acessar seus arquivos.", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_GALLERY_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri filePath = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(filePath);
                imageBitmap = BitmapFactory.decodeStream(inputStream);
                _imageUpload.setImageBitmap(imageBitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void onStoreSuccess() {
        this.presentToast("Mutante salvo com sucesso!");
        onBackPressed();
    }

    private void onStoreError() {
        this.presentToast("Erro ao salvar mutante!");
    }


    private void presentToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private boolean validate() {
        boolean isValid = true;

        String name = _inputName.getText().toString();
        String ab1 = _ability1.getText().toString();
        String ab2 = _ability2.getText().toString();
        String ab3 = _ability3.getText().toString();

        if (name.isEmpty()) {
            _inputName.setError("Nome é obrigatório!");
            isValid = false;
        } else {
            _inputName.setError(null);
        }

        if (ab1.isEmpty() && ab2.isEmpty() && ab3.isEmpty()) {
            this.presentToast("Você deve adicionar pelo menos uma habilidade");
            isValid = false;
        }

        if (this.imageBitmap == null) {
            this.presentToast("Escolha uma imagem para o seu mutante!");
            isValid = false;
        }

        return isValid;
    }

    private String imageToBase64(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

        byte[] imageBytes = outputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
