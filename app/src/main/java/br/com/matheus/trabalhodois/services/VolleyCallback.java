package br.com.matheus.trabalhodois.services;

public interface VolleyCallback {
    void onSuccess(Integer code, String message);
    void onError(int code);
}
