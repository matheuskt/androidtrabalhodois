package br.com.matheus.trabalhodois.services;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.matheus.trabalhodois.entity.Mutant;

public class MutantsRest {

    public void delete(final VolleyCallback callback, final Activity activity, Mutant mutant) {
        try {

            Gson gson = new Gson();
            String json = gson.toJson(mutant);
            JSONObject object = new JSONObject(json);

            Log.d("REQUEST", json);

            final JsonObjectRequest jsonRequest = new SecureRequest(Request.Method.DELETE, "mutant/"+mutant.getId(), object , activity,
                    new Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                callback.onSuccess(200, response.toString());
                            } catch (Exception e) {
                                callback.onError(10);
                            }
                        }
                    },
                    new ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error.networkResponse != null)
                                callback.onError(error.networkResponse.statusCode);
                            else
                                callback.onError(10);
                        }
                    }
            ) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String json = new String(response.data);
                        JSONObject jsonResponse = new JSONObject(json);

                        return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }
            };

            SingletonRequestQueue.getInstance(activity).addToRequestQueue(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Erro", "Erro sei la no q");
        }
    }

    public void store(final VolleyCallback callback, final Activity activity, Mutant mutant) {
        try {

            Gson gson = new Gson();
            String json = gson.toJson(mutant);
            JSONObject object = new JSONObject(json);

            Log.d("REQUEST", json);

            final JsonObjectRequest jsonRequest = new SecureRequest(Request.Method.POST, "mutant", object , activity,
                    new Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                callback.onSuccess(200, response.toString());
                            } catch (Exception e) {
                                callback.onError(10);
                            }
                        }
                    },
                    new ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error.networkResponse != null)
                                callback.onError(error.networkResponse.statusCode);
                            else
                                callback.onError(10);
                        }
                    }
            ) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String json = new String(response.data);
                        JSONObject jsonResponse = new JSONObject(json);

                        return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }
            };

            SingletonRequestQueue.getInstance(activity).addToRequestQueue(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Erro", "Erro sei la no q");
        }
    }

    public void search(final VolleyCallback callback, final Activity activity, String term) {
        try {

            final JsonObjectRequest jsonRequest = new SecureRequest(Request.Method.GET, "user/mutants/search?term=" + term, activity,
                    new Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                callback.onSuccess(200, response.toString());
                            } catch (Exception e) {
                                callback.onError(10);
                            }
                        }
                    },
                    new ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error.networkResponse != null)
                                callback.onError(error.networkResponse.statusCode);
                            else
                                callback.onError(10);
                        }
                    }
            ) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String json = new String(response.data);
                        JSONArray arrResponse = new JSONArray(json);
                        JSONObject jsonResponse = new JSONObject();
                        jsonResponse.put("mutants", arrResponse);

                        return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }
            };

            SingletonRequestQueue.getInstance(activity).addToRequestQueue(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Erro", "Erro sei la no q");
        }
    }

    public void list(final VolleyCallback callback, final Activity activity) {

        try {

            final JsonObjectRequest jsonRequest = new SecureRequest(Request.Method.GET, "user/mutants", activity,
                    new Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                callback.onSuccess(200, response.toString());
                            } catch (Exception e) {
                                callback.onError(10);
                            }
                        }
                    },
                    new ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(error.networkResponse != null)
                                callback.onError(error.networkResponse.statusCode);
                            else
                                callback.onError(10);
                        }
                    }
            ) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String json = new String(response.data);
                        JSONArray arrResponse = new JSONArray(json);
                        JSONObject jsonResponse = new JSONObject();
                        jsonResponse.put("mutants", arrResponse);

                        return Response.success(jsonResponse, HttpHeaderParser.parseCacheHeaders(response));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }
            };

            SingletonRequestQueue.getInstance(activity).addToRequestQueue(jsonRequest);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d("Erro", "Erro sei la no q");
        }
    }

}
