package br.com.matheus.trabalhodois.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.com.matheus.trabalhodois.R;
import br.com.matheus.trabalhodois.entity.Login;
import br.com.matheus.trabalhodois.services.LoginRest;
import br.com.matheus.trabalhodois.services.TokenManagement;
import br.com.matheus.trabalhodois.services.VolleyCallback;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    private LoginRest loginRest;
    private Login login;

    @BindView(R.id.btnLogin) Button _btnLogin;
    @BindView(R.id.inputEmail) TextView _inputEmail;
    @BindView(R.id.inputPassword) TextView _inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        loginRest = new LoginRest();
        login = new Login();

        _btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    public void login() {
        Log.d("ActivityLogin", "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }

        _btnLogin.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Realizando login...");
        progressDialog.show();

        login.setEmail(_inputEmail.getText().toString());
        login.setPassword(_inputPassword.getText().toString());

        loginRest.login(new VolleyCallback() {
            @Override
            public void onSuccess(Integer code, String token) {
                progressDialog.dismiss();
                onLoginSuccess(token);
            }

            @Override
            public void onError(int code) {
                progressDialog.dismiss();
                onLoginFailed();

                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Erro!").setIcon(android.R.drawable.ic_dialog_alert);

                if (code == 401) builder.setMessage("E-mail ou senha inválidos!");
                else builder.setMessage("Aconteceu algum erro ao tentar realizar seu login");

                builder.show();
            }
        }, this, login);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess(String Token) {
        _btnLogin.setEnabled(true);

        TokenManagement managementService = new TokenManagement(this);
        managementService.saveToken(Token);

        Intent it = new Intent(LoginActivity.this, DashboardActivity.class);
        startActivity(it);
        finish();
    }

    public void onLoginFailed() {
        _btnLogin.setEnabled(true);
    }

    public boolean validate() {
        boolean isValid = true;

        String email = _inputEmail.getText().toString();
        String password = _inputPassword.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _inputEmail.setError("E-mail inválido!");
            isValid = false;
        } else {
            _inputEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 6) {
            _inputPassword.setError("A senha deve possuir no mínimo 6 caracteres.");
            isValid = false;
        } else {
            _inputPassword.setError(null);
        }

        return isValid;
    }
}
