package br.com.matheus.trabalhodois.entity;

import java.io.Serializable;

public class Ability implements Serializable {

    int id;
    String name;

    public Ability(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
