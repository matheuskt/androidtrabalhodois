package br.com.matheus.trabalhodois.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class TokenManagement {

    private static final String TOKEN = "USER_TOKEN";

    private Context mContext;

    public TokenManagement(Context context) {
        this.mContext = context;
    }

    public void saveToken(String Token) {
        SharedPreferences settings = mContext.getSharedPreferences(TOKEN, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TOKEN, Token);

        // Commit the edits!
        editor.commit();
    }

    public String getToken() {
        SharedPreferences settings = mContext.getSharedPreferences(TOKEN, 0);
        return settings.getString(TOKEN, "");
    }

    public void deleteToken() {
        SharedPreferences mySPrefs = mContext.getSharedPreferences(TOKEN, 0);
        SharedPreferences.Editor editor = mySPrefs.edit();
        editor.remove(TOKEN);
        editor.apply();
    }
}