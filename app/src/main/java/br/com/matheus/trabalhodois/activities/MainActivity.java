package br.com.matheus.trabalhodois.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import br.com.matheus.trabalhodois.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
