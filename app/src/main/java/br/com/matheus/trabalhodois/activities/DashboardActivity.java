package br.com.matheus.trabalhodois.activities;

import androidx.appcompat.app.AppCompatActivity;
import br.com.matheus.trabalhodois.R;
import br.com.matheus.trabalhodois.services.TokenManagement;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }

    public void onCreateClick(View view) {
        Intent it = new Intent(this, CreateActivity.class);
        startActivity(it);
    }

    public void onListClick(View view) {
        Intent it = new Intent(this, ListActivity.class);
        startActivity(it);
    }

    public void onSearchClick(View view) {
        Intent it = new Intent(this, SearchActivity.class);
        startActivity(it);
    }

    public void onLogoutClick(View view) {
        TokenManagement tokenManagement = new TokenManagement(this);
        tokenManagement.deleteToken();
        finish();
    }
}
